SPARK_BIN_PATH=/Users/seri/Documents/scala-work/spark-2.1.0-bin-hadoop2.7/bin
MASTER=local[4]

build:
	sbt package

exec:
	$(SPARK_BIN_PATH)/spark-submit --master $(MASTER) --class "SimpleApp" target/scala-2.11/spark_sample_2.11-1.0.jar

version:
	$(SPARK_BIN_PATH)/spark-submit --version
